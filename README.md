###The problem

Mailtrack has decided to implement a Spam Rating Tool.

The rules it follows are as following:
- A mail of 100 chars or shorter will receive 20 points.
- A mail of 500 chars or more will receive 50 points.

Aditionally to this, it will get 5 points for each image found in the email body and 10 points for each link found in the email body.

However, if the user has already sent an email to the sender, it will not be considered as spam, getting 0 points.
 
###Your Solution

- You must implement a Spam Rating Tool that follows these rules.
- A data set mails.json is provided. You must pass the validation test using this data set. Fill "yourCodeGoesHere" function with any calls you need to your code.
- Your solution must pass the ValidationTest provided. You can run the test with Docker if you want:
    - docker-compose run php composer install 
    - docker-compose run php vendor/bin/phpunit tests/ValidationTest.php

###We will look positively to:

* The design of your unit tests
* Code is easy to change 
* Coding principles used (SOLID, design patterns, architectural patterns ...) 


Examples:

* id 3 should return 50
* id 10 should return 0
* id 25 should return 25
* id 40 should return 30
* id 44 should return 15
* id 50 should return 0
* id 60 should return 40


NOTE FROM DEVELOPER: In order to run the tests, please be sure that you already have installed the PHP DOM extensions.
