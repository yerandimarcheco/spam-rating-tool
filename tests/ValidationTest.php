<?php

use PHPUnit\Framework\TestCase;

require __DIR__."/../classes/SpanTool.php";

class ValidationTest extends TestCase
{
    /**
     * @dataProvider mailsProvider
     */
    public function testMails(int $idMail, int $expectedValue): void
    {

        $spamValue = $this->yourCodeGoesHere($idMail);


        $this->assertSame($expectedValue, $spamValue);
    }

    public function testMailNotExisiting(): void
    {
        $this->expectException(\Exception::class);

        $this->yourCodeGoesHere(999);
    }

    public function mailsProvider(): array
    {
        return [
            [
                3,
                50
            ],
            [
                10,
                0
            ],
            [
                25,
                25
            ],
            [
                40,
                30
            ],
            [
                50,
                0
            ],
            [
                60,
                40
            ]
        ];
    }

    private function yourCodeGoesHere(int $idMail): int
    {
        $span = new \classes\SpanTool();
        return $span->getEmailAndPoints($idMail);
    }
}
