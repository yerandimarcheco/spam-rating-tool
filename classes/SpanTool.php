<?php
/**
 * Description: Class to handle the spans. This is the main class of the project.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 22/03/2019
 */
namespace classes;

include_once('LargeRule.php');
include_once('ShortRule.php');
include_once('NoneRule.php');
include_once('Rule.php');

class SpanTool
{
    //List of mail
    private $mailList = [];

    /**
     * Function to get the points by mail id
     * @param integer $mailId
     * @return integer
     * @throws \Exception
    */
    public function getEmailAndPoints($mailId)
    {
        try {
            $this->loadMails();

            $mail = $this->getItemById($mailId);

            return $this->getPoints($mail);
        } catch (\ErrorException $errorException) {
            echo 'Message: ' .$errorException->getMessage();
        }
    }

    /**
     * Function to load the array email form json file. The loaded mail will be added the mailList.
     * @return void
    */
    public function loadMails()
    {
        try {
            $mails = json_decode(file_get_contents(__DIR__ . "/../mails.json"), true);

            foreach ($mails['mails'] as $mail) {
                $this->addMail($mail);
            }
        } catch (\ErrorException $errorException) {
            echo 'Message: ' .$errorException->getMessage();
        }
    }

    /**
     * Function to add one email to the mailList.
     * @param array $mail
     * @return void
     * @return \Exception
    */
    public function addMail($mail)
    {
        try {
            $item = null;
            if (strlen(strip_tags($mail['body'])) <= 100) { //A mail of 100 chars or shorter.
                $item = new ShortRule($mail['id'], $mail['from'], $mail['to'], $mail['body']);
            } elseif (strlen(strip_tags($mail['body'])) >= 500) { //A mail of 500 chars or more.
                $item = new LargeRule($mail['id'], $mail['from'], $mail['to'], $mail['body']);
            } else { //None of rules.
                $item = new NoneRule($mail['id'], $mail['from'], $mail['to'], $mail['body']);
            }

            $this->mailList[$item->getId()] = $item;
        } catch (\ErrorException $errorException) {
            echo 'Message: ' .$errorException->getMessage();
        }
    }

    /**
     * Function to get one mail from the mailList by id
     * @param integer $id
     * @return  Mail
     * @throws \Exception
    */
    public function getItemById($id)
    {
        if (!isset($this->mailList[$id])) {
            throw new \Exception('Unable to find the id');
        }
        return $this->mailList[$id];
    }

    /**
     * Function to get the points of one email
     * @param RuleInterface $mail
     * @return integer
    */
    public function getPoints($mail)
    {
        try {
            $rule = new Rule($mail);
            $points = $rule->getPoint();
            if (!$this->isValidSpan($mail)) {
                $points = 0;
            }
            return $points;
        } catch (\ErrorException $errorException) {
            echo 'Message: ' .$errorException->getMessage();
        }
    }

    /**
     * Function to check is one mail is a valid span
     * @param RuleInterface $mail
     * @return boolean
    */
    public function isValidSpan($mail)
    {
        try {
            $valid = true;
            foreach ($this->mailList as $item) {
                if (($item->getFrom() == $mail->getTo()) && ($item->getTo() == $mail->getFrom())) {
                    $valid = false;
                    break;
                }
            }
            return $valid;
        } catch (\ErrorException $errorException) {
            echo 'Message: ' .$errorException->getMessage();
        }
    }
}
