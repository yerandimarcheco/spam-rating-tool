<?php
/**
 * Description: Class to handle the email rules for those email that don't belong to any rules. This class implement the Rule interface.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 22/03/2019
 */

namespace classes;

include_once ('Mail.php');
include_once ('RuleInterface.php');

class NoneRule extends Mail implements RuleInterface
{
    /**
     * @inheritdoc
    */
    public function getPoint()
    {
        return $this->getExtraPoints();
    }
}
