<?php
/**
 * Description: Class to handle the email rules for those email that body has more than to 500 characters. This class implement the Rule interface.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 22/03/2019
 */

namespace classes;

include_once ('Mail.php');
include_once ('RuleInterface.php');

class LargeRule extends Mail implements RuleInterface
{
    /**
     * @inheritdoc
     * A mail of 500 chars or more will receive 50 points.
    */
    public function getPoint()
    {
        return 50 + ($this->getExtraPoints());
    }
}
