<?php
/**
 * Description: Class email.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 22/03/2019
 */

namespace classes;

class Mail
{
    protected $id;
    protected $from;
    protected $to;
    protected $body;
    private $domDocument;

    /**
     * Class constructor
     * @param integer $id
     * @param string $from
     * @param string $to
     * @param string $body
     * return void
    */
    public function __construct($id, $from, $to, $body)
    {
        $this->id = $id;
        $this->from = $from;
        $this->to = $to;
        $this->body = $body;
        $this->domDocument = new \DOMDocument();
        $this->domDocument->loadHTML($body);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Function to get the image quantity inside the email body
     * @return integer
    */
    public function getImageQuantity()
    {
        $items = $this->domDocument->getElementsByTagName('img');
        return $items->length;
    }

    /**
     * Function to get the link quantity inside the email body
     * @return integer
    */
    public function getLinkQuantity()
    {
        $items = $this->domDocument->getElementsByTagName('a');
        return $items->length;
    }

    /**
     * Function to get the extra points
    */
    public function getExtraPoints()
    {
        return ($this->getImageQuantity() * 5) + ($this->getLinkQuantity() * 10);
    }
}
