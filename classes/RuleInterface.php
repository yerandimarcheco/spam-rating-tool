<?php
/**
 * Description: Interface to handle the points rules. All new rules must be implement this interface.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 22/03/2019
 */

namespace classes;

interface RuleInterface
{
    /**
     * Function to get the point received by email
     * @return integer
    */
    public function getPoint();
}
