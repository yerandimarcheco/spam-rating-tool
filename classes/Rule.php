<?php
/**
 * Description: Class to handle the email. This class has an object type RuleInterface. This class is the context in the strategy pattern.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 22/03/2019
 */

namespace classes;

include_once('RuleInterface.php');

class Rule
{
    //This object will be RuleInterface type
    private $rule;

    /**
     * Class constructor. Get only objects that belong to the Rule interface.
     * @param RuleInterface $rule
     */
    public function __construct(RuleInterface $rule)
    {
        $this->rule = $rule;
    }

    /**
     * Function to get the mail points.
     * @return integer
    */
    public function getPoint()
    {
        return $this->rule->getPoint();
    }
}
