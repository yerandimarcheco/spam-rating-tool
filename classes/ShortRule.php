<?php
/**
 * Description: Class to handle the email rules for those email that body has less to 100 characters. This class implement the Rule interface.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 22/03/2019
 */

namespace classes;

include_once ('Mail.php');
include_once ('RuleInterface.php');

class ShortRule extends Mail implements RuleInterface
{
    /**
     * @inheritdoc
     * A mail of 100 chars or shorter will receive 20 point
    */
    public function getPoint()
    {
        return 20 + ($this->getExtraPoints());
    }
}
